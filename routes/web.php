<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// 404
Route::get('/', function () {
    return view('page.404');
});

Route::get('index', [
	'as'   => 'trang-chu',
	'uses' => 'PageController@getIndex'
]);

Route::get('loai_san_pham/{type?}', [
	'as'  => 'loaisanpham',
	'uses'=> 'PageController@getLoaiSp'
]);

Route::get('chi_tiet_san_pham/{id}',[
	'as'  => 'chitietsanpham',
	'uses'=> 'PageController@getChitiet'
]);

Route::get('lien_he',[
	'as'  => 'lienhe',
	'uses'=> 'PageController@getLienhe'
]);

Route::get('gioi_thieu',[
	'as'  => 'gioithieu',
	'uses'=> 'PageController@getGioithieu'
]);

// cart
Route::get('add-to-cart/{id}', [
	'as'   => 'themgiohang',
	'uses' => 'PageController@getAddtoCart'
]);

Route::get('delete-cart/{id}', [
	'as'   => 'xoagiohang',
	'uses' => 'PageController@getDelItemCart'
]);

Route::get('delete-all-cart', [
	'as'   => 'xoahetgio',
	'uses' => 'PageController@getDelAllCart'
]);

Route::get('dat-hang', [
	'as'   => 'dathang',
	'uses' => 'PageController@getCheckout'
]);
// end cart

Route::post('dat-hang2', [
	'as'   => 'dathang2',
	'uses' => 'PageController@postCheckout'
]);

Route::get('login_form', [
	'as'   => 'formLogin',
	'uses' => 'PageController@viewLogin'
]);

Route::post('login', [
    'as'   => 'postLogin',
	'uses' => 'PageController@postLogin'
]);

Route::get('regis-form', [
	'as'   => 'formRegis',
	'uses' => 'PageController@viewRegis'
]);

Route::post('dang-ky', [
	'as'   => 'signup',
	'uses' => 'PageController@postSignup'
]);
