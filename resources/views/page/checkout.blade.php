@extends('master')
@section('content')
<div class="container">
	<div id="content">
		@if(session('Notification'))
			{{session('Notification')}}
		@endif
		<form action="{{route('dathang2')}}" method="post" class="beta-form-checkout">
			{{ csrf_field() }}
			<div class="row">
				<div class="col-sm-6">
					<h4>Billing Address</h4>
					<div class="space20">&nbsp;</div>

					<div class="form-block">
						<label for="name">First name*</label>
						<input type="text" id="name" name="name" required>
					</div>

					<div class="form-block">
						<label for="gender">Gender*</label>
						<input type="radio" id="gender" class="input-radio" name="gender" value="nam" checked="checked" style="width: 10%"><span style="margin-right: 10%">Nam</span>
						<input type="radio" id="gender" class="input-radio" name="gender" value="nu" checked="checked" style="width: 10%"><span style="margin-right: 10%">Nu</span>
					</div>

					<div class="form-block">
						<label for="email">Email address*</label>
						<input type="email" name="email" id="email" required>
					</div>

					<div class="form-block">
						<label for="address">Address*</label>
						<input type="text" id="address" name="address" value="Street Address" required>
					</div>

					<div class="form-block">
						<label for="phone">Phone*</label>
						<input type="text" id="phone" name="phone" required>
					</div>
					
					<div class="form-block">
						<label for="notes">Order notes</label>
						<textarea id="notes" name="notes"></textarea>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="your-order">
						<div class="your-order-head"><h5>Your Order</h5></div>
						<div class="your-order-body">
							<div class="your-order-item">
								<div>
								@if(session('cart'))
								@foreach($getCart->items as $v)
								<!--  one item	 -->
									<div class="media">
										<img width="35%" src="source/image/product/{{$v['item']['image']}}" alt="" class="pull-left">
										<div class="media-body">
											<p class="font-large">Name: {{$v['item']['name']}}</p>
											<span class="color-gray your-order-info">Price: {{$v['price']}}</span>
											<span class="color-gray your-order-info">Size: M</span>
											<span class="color-gray your-order-info">Quantity: {{$v['qty']}}</span>
										</div>
									</div>
								<!-- end one item -->
								@endforeach
								@endif
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="your-order-item">
								<div class="pull-left"><p class="your-order-f18">Total:</p></div>
								@if(session('cart'))
									<div class="pull-right"><h5 class="color-black">${{$getCart->totalPrice}}</h5></div>
								@else
									<div class="pull-right"><h5 class="color-black">$0</h5></div>
								@endif
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="your-order-head"><h5>Payment Method</h5></div>
						
						<div class="your-order-body">
							<ul class="payment_methods methods">
								<li class="payment_method_bacs">
									<input id="payment_method_bacs" type="radio" class="input-radio" name="payment_method" value="bacs" checked="checked" data-order_button_text="">
									<label for="payment_method_bacs">Direct Bank Transfer </label>
									<div class="payment_box payment_method_bacs" style="display: block;">
										Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won’t be shipped until the funds have cleared in our account.
									</div>						
								</li>

								<li class="payment_method_cheque">
									<input id="payment_method_cheque" type="radio" class="input-radio" name="payment_method" value="cheque" data-order_button_text="">
									<label for="payment_method_cheque">Cheque Payment </label>
									<div class="payment_box payment_method_cheque" style="display: none;">
										Please send your cheque to Store Name, Store Street, Store Town, Store State / County, Store Postcode.
									</div>						
								</li>
								
								<li class="payment_method_paypal">
									<input id="payment_method_paypal" type="radio" class="input-radio" name="payment_method" value="paypal" data-order_button_text="Proceed to PayPal">
									<label for="payment_method_paypal">PayPal</label>
									<div class="payment_box payment_method_paypal" style="display: none;">
										Pay via PayPal; you can pay with your credit card if you don’t have a PayPal account
									</div>						
								</li>
							</ul>
						</div>

						<div class="text-center"><input class="beta-btn primary" type="submit" value='CHECKOUT'/></div>
					</div> <!-- .your-order -->
				</div>
			</div>
		</form>
	</div> <!-- #content -->
</div> <!-- .container -->
@endsection