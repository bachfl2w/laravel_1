@extends('master')
@section('content')
<div class="container">
	<div id="content">
		<form action="{{route('postLogin')}}" method="post" class="beta-form-checkout">
			{{ csrf_field() }}
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6">
					<h4>Đăng nhập</h4>
					<div class="space20">&nbsp;</div>

					
					<div class="form-block">
						<label for="email">Email address*</label>
						<input type="email" id="email" required>
					</div>
					<div class="form-block">
						<label for="phone">Password*</label>
						<input type="text" id="phone" required>
					</div>
					<div class="form-block">
						<input type="submit" class="btn btn-primary" value="Login">
					</div>
				</div>
				<div class="col-sm-3"></div>
			</div>
		</form>
	</div> <!-- #content -->
</div> <!-- .container -->

@endsection