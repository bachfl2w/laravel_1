<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\Cart;
use App\Slide;
use App\Product;
use App\ProductType;
use App\BillDetail;
use App\Bill;
use App\Customer;
use App\User;

use Session;
use Hash;

class PageController extends Controller
{
    public function getIndex()
    {
    	$slide = Slide::all();

    	// select * from `product` where `new` = 1
    	$new_product  = Product::where('new', 1)->paginate(4);
    	$sp_khuyenmai = Product::where('promotion_price', '<>' , 0)->paginate(8);
    	// dd($new_product);
        // dd(session()->all());

    	// return view('page.trangchu', ['slide' => $slide]);
    	return view(
            'page.trangchu',
            compact('slide', 'new_product', 'sp_khuyenmai')
        );
    }

    public function getLoai($type)
    {
        $loai_sp = Product::where('id_type', $type)->get();
    }

    public function getLoaiSp($type)
    {
        $loai_sp     = Product::where('id_type', $type)->get(); // lay nhieu sp cung loai
        $sp_khac     = Product::where('id_type', '<>', $type)->paginate(3);
        $loai        = ProductType::all();
        $ten_loai_sp = ProductType::where('id', $type)->first();

    	return view(
            'page.loai_san_pham',
            compact('loai_sp', 'sp_khac', 'loai', 'ten_loai_sp')
        );
    }

    public function getChitiet(Request $req)
    {
        $sp             = Product::where('id', $req->id)->first();
        $sp_lienquan    = Product::where('id_type', $sp->id_type)->paginate(3);
        $new_product    = Product::where('new', 1)->limit(6)->get();
        $best_seller_id = DB::table('Bill_Detail')
            ->select('id_product')
            ->groupBy('id_product')
            ->orderBy(DB::raw('SUM(Quantity)'), 'desc')
            ->limit(4)
            ->get();

        $id  = '';
        $dem = 0;

        foreach ($best_seller_id as $k => $v) {
            foreach ($v as $k1 => $v1) {

                $dem++;
                
                if ($dem != 2) {
                    $id .= $v1 . ',';
                } else {
                    $id .= $v1;
                }
            }
        }
        $arr         = explode(',', $id);
        $best_seller = Product::whereIn('id', $arr)->get();

    	return view(
            'page.chitiet_sanpham',
            compact('sp', 'sp_lienquan', 'new_product', 'best_seller')
        );
    }

    public function getLienhe()
    {
    	return view('page.lienhe');
    }

    public function getGioithieu()
    {
    	return view('page.gioithieu');
    }

    public function getAddtoCart(Request $req, $id)
    {
        // get by id
        $product = Product::find($id);

        if ($product) {

            $oldCart = Session('cart') ? Session::get('cart') : null ;
            $cart    = new Cart($oldCart);
            $cart->add($product, $id);
            $req->session()->put('cart', $cart);
        }

        return redirect()->back();
    }

    public function getDelItemCart($id)
    {
        $check   = Product::find($id);
        $oldCart = Session::has('cart') ? Session('cart') : null;

        if ($check && $oldCart) {
            $cart = new Cart($oldCart);
            $cart->removeItem($id);

            if (count($cart->items) == 0) {
                session::forget('cart');
            } else {
                session(['cart' => $cart]);
            }
        }

        return back();
    }

    public function getDelAllCart()
    {
        if (session('cart')) {
            $cart = new Cart(session('cart'));
            $cart->removeAllItem();
        }

        return back();
    }

    public function getCheckout()
    {
        $cart = session('cart') ? session('cart') : null;
        return view('page.checkout', ['getCart' => $cart]);
    }

    public function postCheckout(Request $req)
    {
        $cart = session('cart');

        $customer = new Customer();
        // model->`table column` = request->`input name`
        $customer->name         = $req->name;
        $customer->gender       = $req->gender;
        $customer->email        = $req->email;
        $customer->address      = $req->address;
        $customer->phone_number = $req->phone;
        $customer->note         = $req->notes;
        $customer->save(); // save to db

        $bill = new Bill;
        $bill->id_customer = $customer->id;
        $bill->date_order  = date('Y-m-d');
        $bill->total       = $cart->totalPrice;
        $bill->payment     = $req->payment_method;
        $bill->note        = $req->notes;
        $bill->save();

        foreach ($cart->items as $key => $value) {
            $bill_detail = new BillDetail;
            $bill_detail->id_bill    = $bill->id;
            $bill_detail->id_product = $key;
            $bill_detail->quantity   = $value['qty'];
            $bill_detail->unit_price = $value['price'] / $value['qty'];
            $bill_detail->save();
        }

        session()->forget('cart');

        return back()->with('Notification', 'Order Successfully !');
    }

    public function viewLogin()
    {
        return view('page.login');
    }

    public function viewRegis()
    {
        return view('page.signup');
    }

    public function postLogin(Request $req)
    {
        $check = User::where('email');
    }

    public function postSignup(Request $req)
    {
        $this->validate(
            [
                'email'       => 'required | email | unique:users, email',
                'password'    => 'required | min:6 | max:20',
                'fullname'    => 'required',
                're_password' => 'required | same:password'
            ],
            [
                'email.required'    => 'Email',
                'email.email'       => 'Not is a email',
                'email.unique'      => 'Email already exists',
                'password.required' => 'Pass',
                're_password.same'  => 'Pass not same',
                'password.min'      => 'Pass is too short'
            ]
        );

        $user = new User();
        $user->full_name = $req->fullname;
        $user->email     = $req->email;
        $user->password  = Hash::make($req->password);
        $user->phone     = $req->phone;
        $user->address   = $req->address;
        $user->save();

        return back()->with('success', 'Create account success !');
    }
}
