<?php

namespace App;

class Cart
{
	public $items	   = null;
	public $totalQty   = 0;
	public $totalPrice = 0;

	public function __construct($oldCart)
	{
		if($oldCart)
		{
			$this->items 	  = $oldCart->items;
			$this->totalQty   = $oldCart->totalQty;
			$this->totalPrice = $oldCart->totalPrice;
		}
	}

	public function add($product, $id)
	{
		$cart = [
    		'qty'   => 0,
    		'price' => 0,
    		'item'  => $product
    	];

    	if ($this->items) {
    		if (array_key_exists($id, $this->items)) {
    			$cart = $this->items[$id];
    		}
    	}

    	// qty
    	$cart['qty']++;
    	$this->totalQty++;

    	if ($product->promotion_price == 0)
    		$price = $product->unit_price;
    	else
    		$price = $product->promotion_price;

    	// price
    	$cart['price'] = $cart['qty'] * $price; // price foreach item
		$this->totalPrice += $price;

		// item
    	$this->items[$id] = $cart;
	}

	//xóa 1
	public function reduceByOne($id)
	{
		$this->items[$id]['qty']--;
		$this->items[$id]['price'] -= $this->items[$id]['item']['price'];
		$this->totalQty--;
		$this->totalPrice -= $this->items[$id]['item']['price'];

		if($this->items[$id]['qty'] <= 0){
			unset($this->items[$id]);
		}
	}

	//xóa 1 sp
	public function removeItem($id)
	{
		$this->totalQty   -= $this->items[$id]['qty'];
		$this->totalPrice -= $this->items[$id]['price'];

		unset($this->items[$id]);
	}

	// xoa het sp trong gio hang
	public function removeAllItem()
    {
    	session()->forget('cart');
    }
}
