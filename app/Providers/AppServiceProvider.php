<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\ProductType;
use App\Cart;

use Session;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // truyen `loai_sp` len header.blade.php
        view()->composer('header' ,function($view)
        {
            $loai_sp = ProductType::all();

            // load header voi data 'loai_sp duoc truyen vao'
            $view->with('loai_sp', $loai_sp);
        });

        view()->composer('header', function($view) {
            if ( Session('cart') )
            {
                $oldCart = Session::get('cart');
                $cart    = new Cart($oldCart);
                $view->with([
                    'cart'         => Session::get('cart'),
                    'product_cart' => $cart->items,
                    'totalPrice'   => $cart->totalPrice,
                    'totalQty'     => $cart->totalQty
                ]);
            }
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
